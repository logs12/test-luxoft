"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var events_1 = require("events");
var numberOfSensors = 5000;
var intevalStep = 5;
var probeEventName = "probe";
var SensorProbeListener = (function (_super) {
    __extends(SensorProbeListener, _super);
    function SensorProbeListener() {
        _super.apply(this, arguments);
        this.intervalCounter = 1;
        this.identityCounter = 1;
        this.keyGenerator = new KeyNumberGenerator(numberOfSensors);
    }
    SensorProbeListener.prototype.start = function () {
        if (this.interval)
            throw new Error("The listener has already been started.");
        this.interval = setInterval(this.onInterval.bind(this), intevalStep);
    };
    SensorProbeListener.prototype.stop = function () {
        clearInterval(this.interval);
        this.interval = undefined;
    };
    SensorProbeListener.prototype.onInterval = function () {
        this.intervalCounter++;
        var bulkSize = this.getBulkSize();
        if (bulkSize === 0)
            return;
        var time = new Date();
        for (var i = 0; i < bulkSize; i++) {
            var keyNumber = this.keyGenerator.generate();
            var probe = {
                id: this.identityCounter++,
                key: "Sensor-" + ("00000" + keyNumber).slice(-5),
                description: "Lorem ipsum " + keyNumber + " dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                value: Math.round((Math.random() - 0.5) * 100000),
                time: time
            };
            this.emit(probeEventName, probe);
        }
    };
    SensorProbeListener.prototype.getBulkSize = function () {
        if (this.intervalCounter < 10000 / intevalStep) {
            var d = Math.floor(Math.abs(1000 / intevalStep * Math.cos(this.intervalCounter * Math.PI / (20000 / intevalStep)))) + 1;
            return this.intervalCounter % d ? 0 : 1;
        }
        else {
            return 100;
        }
    };
    return SensorProbeListener;
}(events_1.EventEmitter));
exports.SensorProbeListener = SensorProbeListener;
var KeyNumberGenerator = (function () {
    function KeyNumberGenerator(numOfSensors) {
        this.numOfSensors = numOfSensors;
        this.counter = 0;
        this.g = KeyNumberGenerator.gaussian(this.numOfSensors / 2, 25);
    }
    KeyNumberGenerator.prototype.generate = function () {
        return this.counter++ < this.numOfSensors * 10 ?
            (Math.floor(Math.random() * this.numOfSensors) + 1) :
            (Math.floor(this.g() + this.numOfSensors / 2) + 25) % this.numOfSensors + 1;
    };
    KeyNumberGenerator.gaussian = function (mean, stdev) {
        var y2;
        var useLast = false;
        return function () {
            var y1;
            if (useLast) {
                y1 = y2;
                useLast = false;
            }
            else {
                var x1, x2, w;
                do {
                    x1 = 2.0 * Math.random() - 1.0;
                    x2 = 2.0 * Math.random() - 1.0;
                    w = x1 * x1 + x2 * x2;
                } while (w >= 1.0);
                w = Math.sqrt((-2.0 * Math.log(w)) / w);
                y1 = x1 * w;
                y2 = x2 * w;
                useLast = true;
            }
            var retval = mean + stdev * y1;
            if (retval > 0)
                return retval;
            return -retval;
        };
    };
    return KeyNumberGenerator;
}());
//# sourceMappingURL=SensorProbeListener.js.map