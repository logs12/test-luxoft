/*
import {SensorProbeListener} from "./SensorProbeListener";
let listener = new SensorProbeListener();
listener.on("probe", (probe:ISensorProbe)=>{
    console.log(`${probe.key}:${probe.value}`);
})
listener.start();
*/


const express   = require('express'),
    MongoClient = require('mongodb').MongoClient,
    db          = require('./config/db'),
    bodyParser  = require('body-parser'),
    app         = express(),
    port        = 3012;

MongoClient.connect(db.url, (err, database) => {
    if (err) {
        return console.log(err);
    }
    require('./app/routes')(app, database);
    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
});




app.get('/', function(req, res) {
    res.send('hello API');
});

app.listen(port, function() {
    console.log('API app start');
});