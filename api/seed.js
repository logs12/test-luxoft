const express   = require('express'),
    MongoClient = require('mongodb').MongoClient,
    db          = require('./config/db'),
    app         = express(),
    port        = 3012;

MongoClient.connect(db.url, (err, database) => {
    if (err) {
        return console.log(err);
    }
    database.collection('testluxoft').insert({
        key: 'sensor-0001',
        value: '0',
        time: Date.now(),
        description: 'Lorem upsum',
    }, (err, results) => {
        if (err) throw err;
        console.log("1 record inserted");
        database.close();
    });
});